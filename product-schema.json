{
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "https://bitbucket.org/teamgeek/lifestory-schemas/raw/master/product-schema.json",
  "title": "LifeStoryBuilderOutput",
  "description": "Output from the LifeStory builder component.",
  "type": ["object"],
  "definitions": {
    "template": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string"
        },
        "elements": {
          "type": "array",
          "items": {
            "type": "string",
            "format": "uuid"
          }
        }
      },
      "required": ["name", "elements"],
      "additionalProperties": false
    },
    "page": {
      "type": "object",
      "properties": {
        "type": {
          "type": "string",
          "enum": ["cover", "page", "spread-page", "wrap-3d"]
        },
        "minCount": {
          "description": "Ignored if type is `wrap-3d`",
          "type": "number",
          "minimum": 0,
          "default": 0
        },
        "maxCount": {
          "description": "Ignored if type is `wrap-3d`",
          "type": "number",
          "minimum": 0,
          "default": 0
        },
        "templates": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/template"
          },
          "minItems": 1
        },
        "gutter": {
          "type": "number",
          "description": "The part of the page that will be inside the spine in millimetres",
          "default": 5
        },
        "width": {
          "type": "number",
          "description": "Custom page width (before cutting) in millimetres"
        },
        "height": {
          "type": "number",
          "description": "Custom page height (before cutting) in millimetres"
        },
        "foldOver": {
          "type": "number",
          "description": "Area of the paper that is folded"
        },
        "outputFormat": {
          "type": "string",
          "enum": ["pdf", "jpeg"],
          "default": "pdf"
        },
        "pageProduct": {
          "type": "object",
          "description": "Product variant used for adding additional pages for the product",
          "$ref": "#/definitions/productVariant"
        }
      },
      "required": ["type", "templates"],
      "additionalProperties": false
    },
    "element": {
      "type": "object",
      "properties": {
        "context": {
          "type": "string",
          "description": "Only used if type is `text`.",
          "enum": ["title", "spine"],
          "default": null
        },
        "type": {
          "type": "string",
          "enum": ["text", "image", "block"],
          "default": "block"
        },
        "width": {
          "type": "number"
        },
        "height": {
          "type": "number"
        },
        "x": {
          "type": "number",
          "default": 0
        },
        "y": {
          "type": "number",
          "default": 0
        },
        "rotation": {
          "type": "number",
          "description": "Rotation in degrees between 0 and 360",
          "minimum": 0,
          "maximum": 360,
          "default": 0
        },
        "backgroundColor": {
          "type": "string",
          "description": "HTML compatible color string",
          "examples": ["#FFFFFF", "white", "rgba(0,0,0,0.5)"],
          "default": "transparent"
        },
        "borderColor": {
          "type": "string",
          "description": "HTML compatible color string",
          "examples": ["#FFFFFF", "white", "rgba(0,0,0,0.5)"],
          "default": "transparent"
        },
        "borderWidth": {
          "type": "number",
          "description": "The actual width of the border",
          "default": 0
        },
        "fontFamily": {
          "type": "string",
          "description": "Any default browser font family",
          "default": "Arial, Helvetica, sans-serif"
        },
        "fontWeight": {
          "type": "string",
          "description": "Only used if type is `text`.",
          "enum": [
            "normal",
            "bold",
            "bolder",
            "lighter",
            "100",
            "200",
            "300",
            "400",
            "500",
            "600",
            "700",
            "800",
            "900"
          ],
          "default": "400"
        },
        "fontSize": {
          "type": "string",
          "description": "HTML compatible format",
          "default": "12pt"
        },
        "textAlign": {
          "type": "string",
          "enum": ["left", "right", "center", "justify", "initial", "inherit"],
          "default": "inherit"
        },
        "text": {
          "type": "string",
          "description": "Only used if type is `text`. This can be overridden by the user",
          "default": null
        },
        "source": {
          "type": "string",
          "description": "Source url where the image comes from. Only used if type is `image`",
          "default": null
        },
        "customerReplaceable": {
          "type": "boolean",
          "description": "Only used if type is `text` or `image`. Determines if the user can replace the contents of the element",
          "default": false
        }
      },
      "required": ["width", "height"],
      "additionalProperties": false
    },
    "productVariant": {
      "type": "object",
      "properties": {
        "storefrontId": {
          "type": "string",
          "description": "Product variant ID for Shopify Storefront API",
          "examples": [
            "Z2lkOi8vc2hvcGlmeS9Qcm9kdWN0VmFyaWFudC8zMjA2MjY1MTMzNDc4Nw=="
          ],
          "default": ""
        },
        "gid": {
          "type": "string",
          "description": "Product variant ID for Shopify Admin API",
          "examples": ["gid://shopify/ProductVariant/32062651334787"],
          "default": ""
        },
        "variantName": {
          "type": "string",
          "description": "Product variant name to display when config is loaded. Constructed from product name and product variant name",
          "examples": [
            "Landscape Printed Soft Cover Photo Book - Matt / A4 Landscape",
            "Landscape Printed Soft Cover Photo Book - Matt / A4 Portrait"
          ],
          "default": ""
        }
      },
      "required": [""],
      "additionalProperties": false
    }
  },
  "properties": {
    "type": {
      "type": "string",
      "enum": ["photo-book", "model-3d"],
      "default": "photo-book"
    },
    "source": {
      "type": "string",
      "description": "Url of the 3d model (only used if type is `model-3d`",
      "default": null
    },
    "title": {
      "type": "string"
    },
    "description": {
      "type": "string"
    },
    "resolution": {
      "type": "number",
      "description": "Print resolution in DPI",
      "default": 300
    },
    "width": {
      "type": "number",
      "description": "Default product width (before cutting) in millimetres",
      "default": 110
    },
    "height": {
      "type": "number",
      "description": "Default product height (before cutting) in millimetres",
      "default": 110
    },
    "bleed": {
      "type": "number",
      "description": "Area of the paper that is cut off to size in millimetres",
      "default": 2
    },
    "safeArea": {
      "type": "number",
      "description": "The area on the screen that there should not be elements",
      "default": 10
    },
    "spine": {
      "type": "number",
      "description": "The size of the spine in millimetres",
      "default": 5
    },
    "pages": {
      "type": "array",
      "minItems": 1,
      "items": {
        "$ref": "#/definitions/page"
      }
    },
    "elementsById": {
      "type": "object",
      "patternProperties": {
        "^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$": {
          "$ref": "#/definitions/element"
        }
      },
      "additionalProperties": false
    },
    "$schema": {
      "type": "string"
    },
    "defaultLayout": {
      "type": "array",
      "description": "List of pages that match up with the product config",
      "items": {
        "type": "array",
        "description": "List of pages",
        "items": {
          "type": "object",
          "description": "Default template for this particular page",
          "properties": {
            "template": {
              "type": "number",
              "description": "The index of the default selected template for this page",
              "default": 0
            }
          },
          "additionalProperties": false
        }
      }
    },
    "addons": {
      "type": "array",
      "description": "An array of Shopify Product Variant Storefront IDs that will be presented as add ons",
      "items": {
        "$ref": "#/definitions/productVariant"
      }
    }
  },
  "required": ["pages", "defaultLayout"],
  "additionalProperties": false
}
